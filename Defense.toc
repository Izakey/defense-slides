\beamer@endinputifotherversion {3.33pt}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{Literature Review Of Geometric Modeling}{5}{0}{2}
\beamer@subsectionintoc {2}{1}{Overview}{5}{0}{2}
\beamer@subsectionintoc {2}{2}{Wireframe Modeling}{5}{0}{2}
\beamer@subsectionintoc {2}{3}{Surface Modeling}{6}{0}{2}
\beamer@subsectionintoc {2}{4}{Solid Modeling}{9}{0}{2}
\beamer@subsectionintoc {2}{5}{Non-manifold Modeling }{12}{0}{2}
\beamer@sectionintoc {3}{Analysis \& Design}{13}{0}{3}
\beamer@sectionintoc {4}{Results \& Discussion}{26}{0}{4}
